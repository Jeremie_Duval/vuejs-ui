# sl-frontend-vue

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Build Docker image
```
docker build -t sirsteward/rb-panthera-vuejs:latest .
```

### Pull Docker image
```
docker pull sirsteward/rb-panthera-vuejs:latest
```

### Deploy Docker container with console
```
docker run -it -p 8080:8080 --name vue sirsteward/rb-panthera-vuejs:latest
```

### Deploy Docker container without console
```
docker run -p 8080:8080 --name vue sirsteward/rb-panthera-vuejs:latest
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
