const messages = {
  Français: {
    back: "Retour",
    goto_goal: "Déplacer vers une station",
    confirm_goto_goal:
      "Voulez-vous déplacer le robot vers &laquo; %station% &raquo;",
    confirm_dock: "Voulez-vous retournez à la station de charge ?",
    charge_station: "Station de charge",
    confirm_yes: "Oui",
    confirm_no: "Non",
    cancel: "Annuler",
    select_destination: "Sélectionnez une destination",
  },
  English: {
    back: "Back",
    goto_goal: "Move to station",
    confirm_goto_goal:
      "Do you want to move the robot to station &laquo; %station% &raquo;",
    confirm_dock: "Do you want to move to the charge station ?",
    charge_station: "Charge Station",
    confirm_yes: "Yes",
    confirm_no: "No",
    cancel: "Cancel",
    select_destination: "Select destination",
  },
};

export default {
  messages: messages,
};
