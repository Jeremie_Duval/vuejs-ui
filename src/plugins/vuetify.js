import Vue from "vue";
import Vuetify from "vuetify";
import "@mdi/font/css/materialdesignicons.min.css";

Vue.use(Vuetify);

export default new Vuetify({
    theme: { dark: true },
    icons: {
        iconfont: "md"
    }
})
