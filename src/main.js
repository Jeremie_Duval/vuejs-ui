import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import vuetify from "./plugins/vuetify";
import socketio from "socket.io-client";
import VueSocketIO from "vue-socket.io";
import VuetifyConfirm from "vuetify-confirm";

/* Internalization (i18n) */
import VueI18n from "vue-i18n";

import translate from "./translate";

import VueSweetalert2 from "vue-sweetalert2";

// If you don't need the styles, do not connect
import "sweetalert2/dist/sweetalert2.min.css";

import VueMask from "v-mask";

Vue.use(VueMask);

Vue.use(VueSweetalert2);

Vue.config.productionTip = false;

export const SocketInstance = socketio(
  process.env.VUE_APP_SOCKETIO_HOST || "http://localhost:8000",
  {
    query: {
      token: window.localStorage.getItem("auth"),
    },
  }
);

Vue.use(VueI18n);

Vue.use(VuetifyConfirm, { vuetify });

Vue.use(
  new VueSocketIO({
    debug: true,
    connection: SocketInstance,
  })
);

const messages = translate.messages;

const i18n = new VueI18n({
  locale: "Français",
  messages,
});

export const bus = new Vue();

new Vue({
  router,
  vuetify,
  render: (h) => h(App),
  i18n,
}).$mount("#app");
